Syrup Store Agent
=================

이 문서는 POS(Point of Sale)에서 Syrup Store Agent(이하 ``SSA`` )를 이용하여 SK Planet(이하 ``SKP`` )에서 제공하는 서비스를 연동 하기 위한 규격을 정의하고 있다.

.. image:: static/ssa.png
   :width: 70 %
   :align: center


* POS는 ``SSA`` 를 이용하여 ``SKP`` 에서 제공하는 서비스 (``OCB``, ``Coupon``, ``Mileage``, ``Syrup Order``, ``Stamp`` 등)를 이용 할 수 있다.
* POS는 ``SKP`` 의 서비스를 활용하기 위하여 이 문서에서 정의하고 있는 ``SSA`` 연동 규격을 준수하여야 한다.

.. _overview:

|

목 차
---------------

.. toctree::
   :maxdepth: 2

   overview/overview
   protocol/index
   sample/index
   appendix/appendix

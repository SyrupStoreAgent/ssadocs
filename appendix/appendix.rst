########
APPENDIX
########

.. _appendixA:

Appendix A
==========

|

**서비스 별 코드 / 이름 정의**

+-------------+-------------+-------------+
|Service      | ServiceCode | ServiceName |
+-------------+-------------+-------------+
|Common       |SKP0000      |ssa          |
+-------------+-------------+-------------+
|OK Cashbag   |SKP1000      |ocb          |
+-------------+-------------+-------------+
|Coupon       |SKP2000      |coupon       |
+-------------+-------------+-------------+
|Syrup Order  |SKP3000      |syruporder   |
+-------------+-------------+-------------+
|Syrup Mileage|SKP4000      |mileage      |
+-------------+-------------+-------------+
|Syrup Stamp  |SKP5000      |stamp        |
+-------------+-------------+-------------+
|Syrup 복합   |SKP9999      |syrup        |
+-------------+-------------+-------------+

|
|

.. _appendixB:

Appendix B
==========
|

**서비스 타입**

+----------+----------------+
|Type      |ServiceType     |
+----------+----------------+
|호출      |common          |
+----------+----------------+
|적립      |save            |
+----------+----------------+
|사용      |use             |
+----------+----------------+
|할인      |discount        |
+----------+----------------+
|취소      |cancel          |
+----------+----------------+
|주문      |order           |
+----------+----------------+
|재출력    |reprint         |
+----------+----------------+
|결제      |pay             |
+----------+----------------+
|복합      |mix             |
+----------+----------------+
|조회      |query           |
+----------+----------------+

|
|

.. _appendixC:

Appendix C
==========
|

**공통 에러 코드 정의**

+---------+------------+-------------------------------------------------------------------+
|         |error code  | description                                                       |
+---------+------------+-------------------------------------------------------------------+
|SUCCESS  |0           |성공                                                               |
+---------+------------+-------------------------------------------------------------------+
|         |-1          |실패                                                               |
+         +------------+-------------------------------------------------------------------+
|FAILURE  |-2          |POS와 연동 작업중인 거래가 존재함                                  |
+         +------------+-------------------------------------------------------------------+
|         |-3          |사용자에 의한 취소                                                 |
+---------+------------+-------------------------------------------------------------------+
|         |1000        |Device 대표 에러                                                   |
|         +-------+----+-------------------------------------------------------------------+
|         |       |1100|프린터 에러 (대표 에러)                                            |
|         |       +----+-------------------------------------------------------------------+
|         |printer|1101|프린터 용지 교체                                                   |
|         |       +----+-------------------------------------------------------------------+
|         |       |1102|프린터 점검                                                        |
|         +-------+----+-------------------------------------------------------------------+
|         |barcode|1200|바코드 에러 (대표 에러)                                            |
|         +-------+----+-------------------------------------------------------------------+
|Device   |       |1300|MSR 에러 (대표 에러)                                               |
|         |       +----+-------------------------------------------------------------------+
|         |MSR    |1301|MSR port 연결 실패                                                 |
|         |       +----+-------------------------------------------------------------------+
|         |       |1302|MSR 데이터 수신 실패                                               |
|         +-------+----+-------------------------------------------------------------------+
|         |       |1400|서명패드 에러 (대표 에러)                                          |
|         |       +----+-------------------------------------------------------------------+
|         |signpad|1401|서명패드 연결 실패                                                 |
|         |       +----+-------------------------------------------------------------------+
|         |       |1402|서명패드 데이터 수신 실패                                          |
+---------+-------+----+-------------------------------------------------------------------+
|         |2000        |Database 에러 (대표 에러)                                          |
|         +------------+-------------------------------------------------------------------+
|         |2001        |Create 실패                                                        |
|         +------------+-------------------------------------------------------------------+
|Database |2002        |Read 실패                                                          |
|         +------------+-------------------------------------------------------------------+
|         |2003        |Write 실패                                                         |
|         +------------+-------------------------------------------------------------------+
|         |2004        |Delete 실패                                                        |
+---------+------------+-------------------------------------------------------------------+
|         |3000        |네트워크 에러                                                      |
|Network  +------------+-------------------------------------------------------------------+
|         |3001        |네트워크 연결 실패                                                 |
+---------+------------+-------------------------------------------------------------------+
|         |4000        |Service 연동 오류                                                  |
|         +------------+-------------------------------------------------------------------+
|         |4001        |품목코드 오류 (알 수 없는 아이템 코드)                             |
|         +------------+-------------------------------------------------------------------+
|         |4002        |허용되지 않는 요청 (취소 불가 등)                                  |
|Service  +------------+-------------------------------------------------------------------+
|         |4003        |알 수 없는 serviceTrKey (POS에 존재하지 않는 주문에 대한 처리 요청)|
|         +------------+-------------------------------------------------------------------+
|         |4004        |유효하지 않은 serviceTrKey                                         |
|         +------------+-------------------------------------------------------------------+
|         |4100        |영업 중지,종료 등으로 주문 접수 불가                               |
+---------+------------+-------------------------------------------------------------------+

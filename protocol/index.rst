########
PROTOCOL
########

이 문서는 Syrup Store Agent와 연동하기 위한 프로토콜의 구조와 각 필드에 대한 설명을 담고 있다.


Syrup Store Agent에서 사용하고 있는 프로토콜의 전체 구조는 다음과 같다::


  header: {
     transactionNumber(M): 
     protocolVersion(M):
     source(M):
     destination(M):
     contentType(M):
     opCode(M): {
        type(M):
        command(M):
     },
     responseCode(O): {
        code(M):
        message(O):
     }
  }, 
  body: {
    pdu(M): {
       ...
    },
    extra(O): {
       ...
    }
  }

.. toctree::
   :maxdepth: 2

   header.rst
   body.rst

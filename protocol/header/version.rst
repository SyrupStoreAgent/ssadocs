portocolVersion
-----------------
Syrup Store Agent 연동을 위한 프로토콜 버전을 명시한다.

.. warning::

    * **상대 device가 사용하고 있는 프로토콜 버전과 무관하게 현재 사용하고 있는(구현 시점의) 프로토콜 버전을 static 하게 이용한다.**
    * **Syrup Store Agent의 요청/응답에 사용되고 있는 protocolVersion과 별도로 POS측에서 연동개발시 전달 받은 protocol버전을 사용한다.**
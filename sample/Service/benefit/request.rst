############
거래 요청
############


+-------------------------------------+------------+---+----------------------------------------+
|field name                           |data type   |M/O|Description                             |
+-------------------------------------+------------+---+----------------------------------------+
|deviceTrKey                          |string      |O  |POS 에서 생성된 거래번호                |
+-------------------------------------+------------+---+----------------------------------------+
|serviceCode                          |string      |M  |서비스코드, :ref:`appendixA` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|serviceType ``v2.0.0`` ``new``       |string      |M  |서비스타입, :ref:`appendixB` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|amount ``v2.0.0`` ``new``            |number      |O  |거래의 원 금액                          |
+-------------------------------------+------------+---+----------------------------------------+
|authType ``v2.0.0`` ``new``          |string      |O  |인증 정보 타입                          |
+-------------------------------------+------------+---+----------------------------------------+
|authValue ``v2.0.0`` ``new``         |string      |O  |인증 정보 데이터                        |
+-------------------------------------+------------+---+----------------------------------------+

.. _syrup_request:

시럽 혜택 요청
====================

시럽 혜택 요청은 ``POS`` 에서 각 서비스별(``ocb``, ``coupon``, ``mileage``) 버튼이 존재 하지 않고 Syrup 버튼만을 구현한 상황에서 사용되는 호출이다.
``POS`` 에서는 고객의 거래 금액만을 ``SSA`` 로 전달하고, ``SSA`` 에서 적용할 혜택을 처리한 후 ``POS`` 에게 적용된 서비스와 그 결과(적립, 사용)를 전달한다.

* 고객이 현재 거래에서 ``Syrup`` 에서 발급된 ``coupon`` 을 사용하여 할인을 받기 원함.
* ``POS`` 에서 ``SSA`` 를 호출 하여 ``coupon`` 혜택을 적용을 요청하는 경우.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceCode": "SKP0000",
        "serviceType": "common",
        "amount": 12000,
        "authType": "skip",
        "authValue": "skip"
    }

* :ref:`syrup_response`
* :ref:`syrup_cancel_request`
* :ref:`syrup_cancel_response`


|
|

.. note::

    서비스별 요청은 ``POS`` 에서 각 서비스별(``ocb``, ``coupon``, ``mileage``) 버튼이 존재하여 ``SSA`` 에게 지정된 서비스에 대한 처리를 요청하는 경우 사용된다.

.. _ocb_save_request:

OCB 적립 요청
=====================

* 고객이 현재 거래에서 ``ocb`` 를 적립하기 원함.
* 이 경우 ``POS`` 에서 고객의 전화번호 또는 ``ocb`` 카드번호를 입력 받아서 전달해야 함.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceCode": "SKP1000",
        "serviceType": "save",
        "amount": 12000,
        "authType": "mdn",
        "authValue": "010-1234-5678"
    }

* :ref:`ocb_save_response`
* :ref:`ocb_save_cancel_request`
* :ref:`ocb_save_cancel_response`

|

.. _mileage_request:

Mileage 거래 요청
========================

* 고객이 현재 거래에서 ``mileage`` 서비스(적립 또는 사용)를 사용하기 원함.
* 이 경우 ``POS`` 에서 고객의 정보를 입력 받아서 전달해야 함.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceCode": "SKP4000",
        "serviceType": "common",
        "amount": 12000,
        "authType": "mdn",
        "authValue": "010-1234-5678"
    }

* :ref:`mileage_response`
* :ref:`mileage_cancel_request`
* :ref:`mileage_cancel_response`


##################
거래 오류 처리
##################


.. _pending_response:

처리중 새로운 요청이 들어 올 경우
=====================================

.. warning::

    * ``POS`` 와의 연동은 복수개의 처리가 불가능하며, 하나의 거래가 완료되기 이전에 새로운 거래에 대한 요청이 들어오면 ``SSA`` 는 새로운 거래에 대한 요청을 무시한다.
    * 기존 처리중인 거래가 존재 할 경우 ``SSA`` 는 결과 값으로 ``-2`` 를 되돌려 준다.

::

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey....",
        "serviceTrKey": "sample_mileage_transactionKey....",
        "serviceName": "mileage",
        "serviceCode": "SKP4000",
        "serviceType": "use",
        "result": -2
    }

|
|

사용자에 의한 거래 취소
==========================

.. note::

    * 사용자에 의해 거래가 취소 될 경우 ``SSA`` 는 결과 값으로 ``-3`` 을 전달한다.
    * 이 경우 POS는 요청한 Syrup에 대한 처리를 초기화 한다.

::

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey....",
        "serviceTrKey": "sample_mileage_transactionKey....",
        "serviceName": "mileage",
        "serviceCode": "SKP4000",
        "serviceType": "use",
        "result": -3
    }
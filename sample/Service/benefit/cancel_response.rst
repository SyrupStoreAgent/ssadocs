###############
거래 취소 응답
###############


+-------------------------------------+------------+---+----------------------------------------+
|field name                           |data type   |M/O|Description                             |
+-------------------------------------+------------+---+----------------------------------------+
|deviceTrKey                          |string      |O  |POS 에서 생성된 거래번호                |
+-------------------------------------+------------+---+----------------------------------------+
|serviceTrKey                         |string      |O  |서비스에서 생성된 거래번호              |
+-------------------------------------+------------+---+----------------------------------------+
|serviceCode                          |string      |M  |서비스코드, :ref:`appendixA` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|serviceType ``v2.0.0`` ``new``       |string      |M  |서비스타입, :ref:`appendixB` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|result                               |number      |O  |결과 코드, :ref:`appendixC` 참고        |
+-------------------------------------+------------+---+----------------------------------------+

.. _syrup_cancel_response:

Syrup 혜택 취소 요청 결과
==============================

* ``coupon`` 의 경우 ``serviceTrKey`` 로 ``coupon 번호`` 를 사용한다.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "coupon_number(9999213215547897)",
        "serviceCode": "SKP2000",
        "serviceType": "cancel",
        "result: 0
    }

* :ref:`syrup_cancel_request`
* :ref:`syrup_request`
* :ref:`syrup_response`

|
|

.. _ocb_save_cancel_response:

ocb 적립 취소 요청 결과
==========================

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "sample_ocb_transactionKey...",
        "serviceCode": "SKP1000",
        "serviceType": "cancel",
        "result": 0
    }

* :ref:`ocb_save_cancel_request`
* :ref:`ocb_save_request`
* :ref:`ocb_save_response`

|
|

.. _mileage_cancel_response:

Mileage 거래 취소 요청 결과
===============================

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "sample_mileage_transactionKey...",
        "serviceCode": "SKP4000",
        "serviceType": "cancel",
        "result": 0
    }

* :ref:`mileage_cancel_request`
* :ref:`mileage_request`
* :ref:`mileage_response`
#################
거래 취소 요청
#################


+-------------------------------------+------------+---+----------------------------------------+
|field name                           |data type   |M/O|Description                             |
+-------------------------------------+------------+---+----------------------------------------+
|deviceTrKey                          |string      |O  |POS 에서 생성된 거래번호                |
+-------------------------------------+------------+---+----------------------------------------+
|serviceTrKey                         |string      |O  |서비스에서 생성된 거래번호              |
+-------------------------------------+------------+---+----------------------------------------+
|serviceCode                          |string      |M  |서비스코드, :ref:`appendixA` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|serviceType ``v2.0.0`` ``new``       |string      |M  |서비스타입, :ref:`appendixB` 참고       |
+-------------------------------------+------------+---+----------------------------------------+
|authType ``v2.0.0`` ``new``          |string      |O  |인증 정보 타입                          |
+-------------------------------------+------------+---+----------------------------------------+
|authValue ``v2.0.0`` ``new``         |string      |O  |인증 정보 데이터                        |
+-------------------------------------+------------+---+----------------------------------------+

.. _syrup_cancel_request:

Syrup 혜택 취소 요청
=====================

* Syrup 혜택을 호출 하여 쿠폰 사용 내역을 취소 요청
* ``coupon`` 의 경우 ``serviceTrKey`` 로 ``coupon 번호`` 를 사용한다.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "coupon_number(9999213215547897)",
        "serviceCode": "SKP2000",
        "serviceType": "cancel"
    }

* :ref:`syrup_cancel_response`
* :ref:`syrup_request`
* :ref:`syrup_response`

|
|

.. _ocb_save_cancel_request:

ocb 적립 취소 요청
===================

* OCB 적립 요청한 내용을 취소 요청.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "sample_ocb_transactionKey...",
        "serviceCode": "SKP1000",
        "serviceType": "cancel"
    }

* :ref:`ocb_save_cancel_response`
* :ref:`ocb_save_request`
* :ref:`ocb_save_response`

|
|

.. _mileage_cancel_request:

Mileage 거래 취소 요청
==========================

* Syrup Mileage 2000포인트 사용내용을 취소 요청.

::

    # POS > SSA

    "pdu": {
        "deviceTrKey": "sample_pos_transactionKey...",
        "serviceTrKey": "sample_mileage_transactionKey...",
        "serviceCode": "SKP4000",
        "serviceType": "cancel"
    }

* :ref:`mileage_cancel_response`
* :ref:`mileage_request`
* :ref:`mileage_response`

###########
Syrup Order
###########

전체 구조는 :ref:`servicetable` 를 참고 한다.

**Syrup Order 주문 JSON 구성시 주의할 점**

::

    아메리카노: 4100원
    할인: 500원
    수량: 2EA
    ----------------------------------

    "itemList": [
        {
            "itemName": "아메리카노(R)",
            "itemCode": "ABC1000",
            "itemQnt": 2,
            "itemPriceA": 8200,
            "itemPriceB": 7200,
        }
    ]

.. warning::

    * itemPriceA가 나타내는 값은 품목 전체의 합이다. (itemQnt 가 복수일 경우 복수 품목 전체 합)
    * itemPriceA = 4100(품목 개별 단가) * 2(itemQnt)
    * itemPriceB = 3600(품목 할인 단가) * 2(itemQnt)

|
|

**Syrup Order 주문 Option 구성시 주의할 점**

::

    아메리카노: 4100원
    할인: 500원
    샷추가(옵션품목): 2샷 (500원 * 2)
    수량: 2EA
    ----------------------------------

    "itemList": [
        {
            "itemName": "아메리카노(R)",
            "itemCode": "ABC1000",
            "itemQnt": 2,
            "itemPriceA": 8200,
            "itemPriceB": 7200,
            "optionList": [
                {
                    "itemName": "샷추가",
                    "itemCode": "ABC0001",
                    "itemQnt": 2,
                    "itemPriceA": 500,
                    "itemPriceB": 500
                }
            ]
        }
    ]

.. warning::

    * optionList[0].itemPriceA가 나타내는 값은 옵션의 개별 가격이다. (itemQnt 가 복수일 경우 복수 품목 전체 합)
    * optionList[0].itemPriceA = 500(옵션 품목 개별 단가)
    * optionList[0].itemPriceB = 500(옵션 품목 할인 단가)


.. toctree::
    :maxdepth: 2

    order
    cancel
    reprint

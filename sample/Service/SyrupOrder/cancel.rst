.. _syrup_cancel:

주문 취소
===========================

취소 요청
--------------

::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "order",
            "destination": "pos",
            "contentType": "",
            "opCode": {
                "type": "REQ",
                "command": "CM_SERVICE"
            }
        },
        "body" : {
            "pdu": {
                "deviceTrKey": "",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3001",
                "serviceVersion": "1.0"
            },
            "extra": {
            }
        }
    }


성공 응답
--------------

::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "pos",
            "destination": "order",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_SERVICE"
            },
            "responseCode" : {
                "code": 0
            }
        },
        "body" : {
            "pdu": {
                "deviceTrKey": "pos_sample_Key_123456789",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3001",
                "serviceVersion": "1.0",
                "result": 0
            },
            "extra": {
            }
        }
    }



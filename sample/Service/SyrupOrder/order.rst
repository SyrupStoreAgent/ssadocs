.. _syrup_order:

주 문
========================

주문 요청
~~~~~~~~~~~~~~
    - 아메리카노 + 샷추가 2잔
    - 아메리카노 1잔

::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "order",
            "destination": "pos",
            "contentType": "",
            "opCode": {
              "type": "REQ",
              "command": "CM_SERVICE"
            }
        },
        "body" : {
            "pdu": {
                "deviceTrKey": "",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3000",
                "serviceVersion": "1.0",
                "extraData": {
                    "print": "order",
                    "additionalString": [
                        "시럽오더 이벤트", "아메리카노 50%"
                    ]
                },
                "priceInfo": {
                    "priceA": 13300,
                    "priceB": 13300
                },
                "itemList": [
                    {
                        "itemName": "아메리카노(R)",
                        "itemCode": "ABC1000",
                        "itemQnt": 2,
                        "itemPriceA": 8200,
                        "itemPriceB": 8200,
                        "optionList": [
                            {
                                "itemName": "샷추가",
                                "itemCode": "ABC0001",
                                "itemQnt": 1,
                                "itemPriceA": 500,
                                "itemPriceB": 500
                            }
                        ]
                    },
                    {
                        "itemName": "아메리카노(R)",
                        "itemCode": "ABC1000",
                        "itemQnt": 1,
                        "itemPriceA": 4100,
                        "itemPriceB": 4100
                    }
                ]
            },
            "extra": {
            }
        }
    }

|

성공 응답
~~~~~~~~~~~~~
::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "pos",
            "destination": "order",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_SERVICE"
            },
            "responseCode" : {
                "code": 0
            }
        },
        "body" : {
            "pdu": {
                "deviceTrKey": "pos_sample_Key_123456789",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3000",
                "serviceVersion": "1.0",
                "result": 0
            },
            "extra": {
            }
        }
    }



실패 응답 (프린터 오류)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "pos",
            "destination": "order",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_SERVICE"
            },
            "responseCode" : {
                "code": 0
            }
        },
        "body" : {
            "pdu": {
                "deviceTrKey": "pos_sample_Key_123456789",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3000",
                "serviceVersion": "1.0",
                "result": 1100
            },
            "extra": {
            }
        }
    }


실패 응답 (pdu format 오류)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

responseCode의 값은 :ref:`header-error-code` 를 참고 한다.

::

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "pos",
            "destination": "order",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_SERVICE"
            },
            "responseCode" : {
                "code": 5    # pdu format 에러
            }
        },
        "body" : {      # priceInfo 정보가 빠진 pdu.
            "pdu": {    # 전달된 pdu를 그대로 돌려 준다.
                "deviceTrKey": "",
                "serviceTrKey": "order_sample_Key123456789",
                "serviceAliasKey": "M1",
                "serviceCode": "SKP3000",
                "serviceVersion": "1.0",
                "extraData": {
                    "print": "order",
                    "additionalString": [
                        "시럽오더 이벤트", "아메리카노 50%"
                    ]
                },
                "itemList": [
                    {
                        "itemName": "아메리카노(R)",
                        "itemCode": "ABC1000",
                        "itemQnt": 2,
                        "itemPriceA": 8200,
                        "itemPriceB": 8200,
                        "optionList": [
                            {
                                "itemName": "샷추가",
                                "itemCode": "ABC0001",
                                "itemQnt": 1,
                                "itemPriceA": 500,
                                "itemPriceB": 500
                            }
                        ]
                    },
                    {
                        "itemName": "아메리카노(R)",
                        "itemCode": "ABC1000",
                        "itemQnt": 1,
                        "itemPriceA": 4100,
                        "itemPriceB": 4100
                    }
                ]
            },
            "extra": {
            }
        }
    }
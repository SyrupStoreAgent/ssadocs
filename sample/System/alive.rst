Health Check (CM_ALIVE)
-----------------------
POS와 Syrup Store Agent간의 health Check를 위한 용도로 사용된다.

.. note::

    * CM_ALIVE command는 header로만 구성되며, body를 가지지 않는다.
    * POS에서 CM_ALIVE에 대한 처리가 불가능한 상태일 경우 responseCode내에 오류 코드를 넣어 되돌려 준다.


::

    # Request (SSA > POS)

    {
        "header" : {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "ssa",
            "destination": "pos",
            "contentType": "",
            "opCode": {
                "type": "REQ",
                "command": "CM_ALIVE"
            }
        }
    }

|

::

    # Response (POS > SSA)

    {
        "header": {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "1.1.0",
            "source": "pos",
            "destination": "ssa",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_ALIVE"
            },
            "responseCode": {
                "code": 0
            }
        }
    }




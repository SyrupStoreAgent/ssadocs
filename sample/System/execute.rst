Status Check (CM_EXECUTE)
-------------------------
Syrup Store Agent와 POS간의 부가적인 상태 정보를 주고 받는데 사용되는 규격이다.

::

    {
        header: {
            ...
        },
        body: {
            pdu: {
                schemeVersion(M):
                method(M):
                scheme(M):
                contents(O): {
                    ...
                }
            }
        },
        extra(O): {
        }
    }


+---------------+-------------+--------------------------------------+
|**Field Name** |**Data Type**|  **Description**                     |
+---------------+-------------+--------------------------------------+
| schemeVersion | string      | 현재 지원하는 scheme 버전            |
+---------------+-------------+--------------------------------------+
| method        | string      | 지정된 scheme을 수행하기 위한 action |
|               |             | (GET, PUT)                           |
+---------------+-------------+--------------------------------------+
| scheme        | string      | 메시지 요청 내용 scheme              |
+---------------+-------------+--------------------------------------+
| contents      | object      | 요청 받은 scheme에 대한 처리 결과    |
+---------------+-------------+--------------------------------------+


+-------------------+----------+---------------------------------------------+
|**scheme version** |**method**|  **Description**                            |
+-------------------+----------+---------------------------------------------+
|                   | GET      | 지정된 scheme을 이용하여 데이터를 요청한다. |
+ 1.2               +----------+---------------------------------------------+
|                   | PUT      | 지정된 값을 전달 한다.                      |
+-------------------+----------+---------------------------------------------+

.. _pos_status:

pos/*/status
~~~~~~~~~~~~

Syrup Store Agent 초기화시(시작시점)에 POS의 상태 정보를 입력 받기 위해 Syrup Store Agent로 부터 POS에게 호출된다.

.. note::

    * 호출 시점: Syrup Store Agent 시작시
    * 호출 주기: Syrup Store Agnet가 시작시에 1회 요청
    * 특징: 정상 응답을 받기 전까지 반복 호출됨

+-----------+-----------------------------------+---------------------------------+
|           |contents                           |                                 |
+version    +---------------+-------------------+Description                      +
|           |key            | value             |                                 |
+-----------+---------------+-------------------+---------------------------------+
|           |protocolVersion|``"2.0.0"``        |현재 사용중인 protocol version   |
+           +---------------+-------------------+---------------------------------+
|           |schemeVersion  |``"1.2"``          |현재 사용중인 scheme version     |
+ v1.2      +---------------+-------------------+---------------------------------+
|           |vendor         |"개발사/가맹점명"  |POS 개발사 및 사용하려는 가맹점명|
+           +---------------+-------------------+---------------------------------+
|           |saleStatus     |"open" or "close"  |매장의 영업 상태                 |
+-----------+---------------+-------------------+---------------------------------+

::

    # Request (SSA > POS)

    {
        "header" : {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "2.0.0",
            "source": "ssa",
            "destination": "pos",
            "contentType": "",
            "opCode": {
                "type": "REQ",
                "command": "CM_EXECUTE"
            }
        },
        "body": {
            "pdu": {
                "schemeVersion": "1.2",
                "method" : "GET",
                "scheme" : "pos/*/status"
            },
            "extra": {
            }
        }
    }

|

::

    # Response (POS > SSA)

    {
        "header" : {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "2.0.0",
            "source": "pos",
            "destination": "ssa",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_EXECUTE"
            },
            "responseCode" : {
                "code": 0
            }
        },
        "body": {
            "pdu": {
                "schemeVersion": "1.2",
                "method" : "GET",
                "scheme" : "pos/*/status",
                "contents": {
                    "vendor": "xxxxx/yyyy",
                    "protocolVersion": "2.0.0",
                    "schemeVersion": "1.2",
                    "saleStatus": "open"
                }
            },
            "extra": {
            }
        }
    }


|
|
|

.. _pos_sales:

pos/sales
~~~~~~~~~
해당 매장의 개점/폐점(매출 마감) 시점을 Syrup Store Agent에게 알려주기 위해 POS에서 Syrup Store Agent에게 전달 된다.

.. note::

    * 호출 시점: POS 프로그램내 매장의 개점/폐점시
    * 호출 주기: 매장 개점/폐점시 1회 (매장 영업중 중간에 POS 프로그램 재 시작시 호출하지 않는다.)
    * 특징: Syrup Store Agent가 미 응답시 재 호출 할 필요가 없다.
          : 폐점(매출 마감)의 경우 처리 시점등으로 인하여 호출이 힘들 경우 무시해도 된다. (개점시는 반드시 1회 호출을 해주어야 한다.)

::

    # Request (POS > SSA)
    {
        "header" : {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "2.0.0",
            "source": "pos",
            "destination": "ssa",
            "contentType": "",
            "opCode": {
                "type": "REQ",
                "command": "CM_EXECUTE"
            }
        },
        "body": {
            "pdu": {
                "schemeVersion": "1.2",
                "method" : "PUT",
                "scheme" : "pos/sales",
                "contents" : {
                    "salesStatus" : "open"
                }
            },
            "extra": {
            }
        }
    }

|

::

    # Response (SSA > POS)

    {
        "header" : {
            "transactionNumber": "SSA20150311152145975",
            "protocolVersion": "2.0.0",
            "source": "ssa",
            "destination": "pos",
            "contentType": "",
            "opCode": {
                "type": "RES",
                "command": "CM_EXECUTE"
            },
            "responseCode" : {
                "code": 0
            }
        },
        "body": {
            "pdu": {
                "schemeVersion": "1.2",
                "method" : "PUT",
                "scheme" : "pos/sales",
                "result" : 0
            },
            "extra": {
            }
        }
    }